**Project code and data for Greenwood et al., 2019, "Coordinated circadian timing 
through the integration of local inputs in Arabidopsis thaliana".**

## Experimental data

**/experimental_data/Fig_N/raw_tracks/...**

Contains the extracted and unprocessed organ-level tracks used for the 
analysis throughout the study. 

Tracks can be found by figure number. For supplementary figures 
associated with a main figure, data can be found by the main figure number. 

/experimental_data/timestamps.xlsx contains the time stamp information 
(minutes from circadian dawn) for each experiment.

**/Greenwood_etal_2019_datafromfigures.xlsx**

Contains the underlying numerical data for the figures. Data in each sheet of
the spreadsheet corresponds to the matching subplot of the figures.

## Model code

**/model_code/Greenwood_etal_model.m**

Contains the MATLAB code for the models presented in Fig 4, 5 and S6-10 Figs.

**/model_code/Greenwood_etal_model.m**

Model template used by Greenwood_etal_model.m. As shown in S6 Fig. 

## Acknowledgments

In addition to the standard MATLAB functions, the following MATLAB File Exchange
submissions were utilised for the making of figures:

- 'Alternative box plot'. Christopher Hummersone. https://uk.mathworks.com/matlabcentral/fileexchange/46545-alternative-box-plot

- 'shadedErrorBar'. Rob Campbell. https://www.mathworks.com/matlabcentral/fileexchange/26311- raacampbell-shadederrorbar

- 'Perceptually uniform colormap'. Ander Biguri. https://uk.mathworks.com/matlabcentral/fileexchange/51986-perceptually-uniform-colormaps

