ic
clear all
%% Parallelise your code. Insert the number of clusters you have available in NUM_CLUSTER. 
% You can find/discover your clusters by following info on: >doc Discover Clusters and Use Cluster Profiles
NUM_RUNS=24;
NUM_CLUSTERS=12;
myCluster = parcluster('local');
myCluster.NumWorkers = NUM_CLUSTERS;
saveProfile(myCluster);
  
spmd % so results are repeatable
    rng(0,'combRecursive');
end

%for runs=1:1%NUM_RUNS %comment out if you want to run parallel computing 
parfor (runs= 1:NUM_RUNS, NUM_CLUSTERS) % use instead of above to run parallel computing
    runs
    stream = RandStream.getGlobalStream();
    stream.Substream = runs;
    %% Set of decisions you can make before running the script: 
    % 1.  choose whether you will run LDLD, LDLL or LL model:
    % if you are running LDLD simulations uncomment lines below:
%     KappaF=1; % this is the light intensity. Set any value greater than 0 
%     T=240;Tmin=120; % this is the time window of the experiment. [Tmin, T]
%     Factor=0; %this tells us whether the phases are in synch at start (Factor=0)) or not. Read more about it on line 50. 
%     name='LDLD_sims';
    %if you are running LDLL simulations uncomment lines below:
%     KappaF = 0; %
%     T=144;Tmin=24; 
%     Factor=0;
%     name='LDLL_sims';
    %if you are running LLLL simulations uncomment lines below:
    KappaF=0; 
    T=240;Tmin=120;
    Factor=1/3;
    name='LLLL_sims';
    %%
    %2. choose how fast the root will grow by 1 pixel 
    rootgrowth = 5;%it grows by one more pixel every 3 hours (time steps). CUrrently that first growth occurs at time=3h. 
    %%
    % 3. Decide on how  many cells are in the root tip
    RootTipNo = 5;
    %%
    %4. choose the initial phase
    % Xt is number of hours after peak time (initial phase)
    % In LL>LL, initial starting phase can be different: pick it from a Normal distribution with mean Xt and standard deviation XT_stdev 
    Xt=13; % starting phase = -(time of first peak - 24)
    Xt_stdev=Factor*(2*pi); % this tells you about the spread of the phases from  0 to 2*pi. Here  standard deviation is Factor*2*pi
    % Set Factor=0 if in LDLL or LDLD. In case case Factor>=1/3,the initial phase will be taken to be uniformly randomly distributed on the interval [0, 2*pi]
    % This is because if you set factor very large (>1/3) you are likely to get such a wide spread that phases essentially look uniformly distributed (once you take mod(phase,2*pi)).
    %%
    %5. Should model have coupling?
    % set strength 0 is there is no coupling else set it positive 
    strength=1; 
    %%
    %6. How spread do you want the periods measurements to be? 
    omega_stdev=0.1;  % omega_stdev=0.1 means that standard deviation will be 10% of the mean frequency value. Frequency=1/Period.
    %%
    %7. are intrinsic periods graded: only use for LL>LL
    %name='LLLL_nocoupling_noise_sims';
    periodgradient=0; %if root is graded make periodgradient=1
    %% Other things you can change: 
    % CotPer, HypPer, RootPer and RTPer are experimentally measured periods of different tissues (Cotyledon, Hypocotyl, Root and Root tip)
    % Per= period of one light-dark cycle (usually 24). 
    % dt= timestep. 
    CotPer=23.82;
    HypPer=25.41;
    RootPer=28.04;
    RTPer=26.90;  
    Per=24;  
    dt = 1/Per;
    %% initalise model plant template: 
    % template from ModelTemplate.mat. It is a matrix of 0's and 1's
    % Nx= number of pixels in x dimension
    % Ny= number of pixels in y dimension
    % OriginalTemplate= template that has been doubled from ModelTemplate.mat and extended to account for root growth. 
    % tipHyp= top pixel of hypocotyl section
    % finalRT= index of lowest root tip cell
    temp=load('ModelTemplate') ;
    mod_template = temp.template;
    
    %remove parts of template that are not part of plant (longitudinal direction)
    [row, col]=find(mod_template==1);
    last=max(row);
    mod_template(last+1:end, :)= []; 
    %and now double the template in size:
    doubleTemplate=kron(mod_template, ones(2,2)); 
    mod_template=doubleTemplate;% double the size
    %MD 13.01.19 add on another column to the centre of the seedling so the root tip will be 5*5 pixels/cells
    mod_template=[ mod_template(:,1:42) mod_template(:,42) mod_template(:,43:end)];
    %
    NxOR=size(mod_template,1);
    NyOR=size(mod_template,2);
    mod_template(NxOR+1:NxOR+(max(T/rootgrowth)+1), 1:NyOR)=0; % add in extra space where the root will grow to your template. 
    Nx=size(mod_template,1);
    Ny=size(mod_template,2);
   
    % determine cell index where the hypocotyl/ root junction is: 
    cells_where_diffs=find(diff(sum(mod_template'))~=0); %find where shape changes (penultimate change in shape is the hypocotyl/ root junction, as there the width of the plant shrinks. 
    Junction= cells_where_diffs(end-1)+1;%this is first root section index on the template below the hypocotyl/root junction
    tipHyp=cells_where_diffs(8)+1; %this is top of hypocotyl  section 
    width_of_Hyp=find(mod_template(Junction-1, :)==1);
    finalRT = max(find(max(mod_template')==1)); %find cell (pixel) index indicating the last cell of the root tip. 
    width_of_Root=find(mod_template(finalRT,:)~=0);%find out thickness of root in terms of number of cells. 
    %% initialise data arrays:
    % kappa=coupling vector 
    % X= data array (phases). Initial phase is X(:, :, 1). 
    % S = coupling information (initialised as matrix of 0's). 
    kappa=strength*ones(1,Nx);
    X=zeros(Nx,Ny,T);
    if Factor>1/3
    X(:, :, 1) = 2*pi*rand(Nx, Ny);
    else
    X(:, :, 1) = mod((Xt*2*pi/Per)*ones(Nx, Ny)+Xt_stdev*randn(Nx, Ny), 2*pi);
    end
    S = zeros(Nx,Ny); 
    %% Intrinsic oscillation frequencies of different tissues
    % PerMatrix is a matrix of all the periods across the plant template. 
    % omega are intrinsic frequencies in different issues. They are noisy.
    
    PerMatrix=normrnd(CotPer,CotPer*omega_stdev, Nx,Ny);
    dims=size(PerMatrix(tipHyp:Junction-1,width_of_Hyp));
    PerMatrix(tipHyp:Junction-1,width_of_Hyp)=normrnd( HypPer,HypPer*omega_stdev,dims(1), dims(2)); % separate hypocotyl 
    
    if periodgradient==1 % root with graded periods
    PerMatrix(Junction:Junction+51, :)=(repmat((HypPer+(RootPer-HypPer)/52*(1:52)),85,1)')+(omega_stdev*diag((HypPer+(RootPer-HypPer)/52*(1:52)))*randn(52,85)); % make periods  increase toward middle of root
    valPer=RootPer- (RootPer-RTPer)/52*(0:1:52);
    PerMatrix(Junction+52:finalRT-RootTipNo, :)=repmat(valPer,85,1)'+(omega_stdev*diag(valPer)*randn(53,85)); %make periods decrease from mid-root to root tip
    else % root we use in the model without graded periods
    dims=size(PerMatrix(Junction:end, :));
    PerMatrix(Junction:end, :) =normrnd( RootPer,RootPer*omega_stdev,dims(1), dims(2)); % the regular info we have
    end
    dims=size(PerMatrix(finalRT-RootTipNo+1:end, :));
    PerMatrix(finalRT-RootTipNo+1:end, :) =normrnd(RTPer,RTPer*omega_stdev,dims(1), dims(2))  ;% root tip  
    omega=2*pi*Per./PerMatrix;
    %% timing of cuts-- use this section of code if yo want to make shoot, root tip or both cuts
    % CutHypTime= timing of cut of the shoot. If no cut set time over T.
    % CutHypoPos= postion the shoot cut. You set this.
    % templateCutHyp= template for the shoot cut plant. 
    % CutRootTime= timing of cut of the root. If no cut set time over T. If
    % root is cut then root will also stop growing (see later parts of the
    % code)
    % CutRootPos= postion the root cut. You set this.
    % templateCutRoot= template for the root cut plant.
    % templateCutRoot= template for the both root and shoot cut plant
    CutHypTime=301;
    CutHypPos=60; 
    templateCutHyp=mod_template;
    templateCutHyp(CutHypPos,:)=0;
    CutRootTime=301;
    CutRootPos=130;
    templateCutRoot=mod_template;
    templateCutRoot(CutRootPos, :)=0;
    templateCutBoth=mod_template;
    templateCutBoth([CutHypPos CutRootPos] , :)=0;
    %% Simulation (using Euler method) 
    % k is time in hours
    % ind and ind_old keep track of when new pixels are added to the template because of root growth
    % TemplateSave is the seedling template for every time point k. It is a matrix of 1s (when cell is plant cells) and NaNs (when not on plant)
    % template_original keeps the original template (k=1). It is a matrix of 1s (when cell is plant cells) and 0s (when not on plant) 
    ind=0;
    ind_old=0;  
    TemplateSave = NaN(size(mod_template,1),size(mod_template,2),T);
    mm_template=mod_template; template_original=mod_template; mm_template(mm_template==0)=NaN; TemplateSave(:, :, 1)=mm_template;
        
    for k=2:T    
        %% nearest neighbour information: 
        for i=1:Nx
            for j=1:Ny
                if mod_template(i,j)~=0 
                A= sin(X(:,:,k-1)- X(i,j, k-1));
                AA=A.*mod_template;
                K=[1 1 1; 1 0 1; 1 1 1]; 
                C=conv2(AA,K,'same');
                S(i,j)=C(i,j); 
                else 
                   S(i,j)=0; 
                end
            end
        end
        %% Kuramoto model: 
        X(:,:,k)=X(:,:,k-1).*mod_template+dt*(omega.*mod_template+diag(kappa)*S+KappaF*-1*sin(2*pi/24*k-X(:,:,k-1))).*mod_template;
        % phase is taken to be modulus 2*pi
        X(:,:,k)= mod(X(:,:,k), 2*pi); 
        %% Root growth: a new root tip cell is added at the bottom. 
        % Template (mod_template) now grows (1 means there are plant cells on the grid, 0 means there is no cell there cell).
        om=omega;
        if mod(k, rootgrowth)==0 & k<CutRootTime % root keeps on growing until root  is cut. 
            ind=floor(k/rootgrowth); 
            mod_template(finalRT+ind, width_of_Root )=1;
            X(finalRT+ind, width_of_Root,k)=X(finalRT+ind-1, width_of_Root,k);% phase of new cells is the same as phase of bottom set of cells in the template  
             if periodgradient==1
            omega(finalRT+ind-RootTipNo, 1:end)=2*pi*Per/RTPer+omega_stdev*2*pi*Per/RootPer*randn(1, size(mod_template,2)); 
            else
             omega(finalRT+ind-RootTipNo, 1:end)=2*pi*Per/RootPer+omega_stdev*2*pi*Per/RootPer*randn(1, size(mod_template,2)); 
            end
        end  
        
       %% plot the change to the frequencies at every time point 
         
        spy(mod_template); hold on
        spy((omega-om).*mod_template, 'r');
        title(['Time=' num2str(k) ' - red line indicates the pixels where the frequency has been changed from the last time point'])
        clf
        % save the new seedling template for every time k. 
        mm_template=mod_template; mm_template(mm_template==0)=NaN;
        TemplateSave(:, :, k)=mm_template;       
    end
    %% Outputs and plots below: 
    % (1) plot of where the 5*5 pixel squares are taken on the plant for the period and phase analysis (only uncomment and plot this if not doing parallel computing). 
    % (2) outputs: periods, phase and Kuramoto order parameters (later saved in a MAT file)
    % (3) kymograph of scaled gene expression. 
    
    % periods and phases are calculated for an area of  5x5 pixels in each tissue. you can plot where these areas are taken from. Note that for the root tip, the area keeps on changing. 
    midCot=(cells_where_diffs(14)-cells_where_diffs(1))/2+ cells_where_diffs(1);
    cotY=[midCot-2:midCot+2]; cotX=[12:16]; %cells in X-Y for cotyledon

    midHyp=(cells_where_diffs(15)-cells_where_diffs(8))/2+ cells_where_diffs(8);
    hypY=[midHyp-2:midHyp+2];%cells in Y dim for hypocotyl. X dim width of root 
    midRoot=((cells_where_diffs(16)-4)-cells_where_diffs(15))/2+ cells_where_diffs(15);
    rootY=[midRoot-2:midRoot+2];%cells in Y dim for root X dim width of root 
    %plot of the regions:  
    spy(template_original); hold on;
    NewTemp=template_original*0; NewTemp(cotY, cotX)=1;NewTemp(hypY, width_of_Root)=1;NewTemp(rootY, width_of_Root)=1;
    final=max(find(max(TemplateSave(:, :, 1)')==1));NewTemp(final-4:final, width_of_Root)=1;
    spy(NewTemp,'r') %add on 5*5 pixel cell patches
    title({'Original plant template (blue)', 'and 5-by-5 pixel tissue patches (red)'}) 
    
    axis tight
    set(gca, 'YLim', [1 200], 'FontSize', 7, 'YTickLabel', [], 'XTickLabel', [])
    set(gcf, 'PaperUnits', 'centimeters', 'InvertHardcopy', 'off', 'Color', 'w',...
    'PaperPosition', [0 0 2000 2600]/300)
    print('template', '-dpdf', '-r300', '-painters')

%==============================================================
    % Expression is the unscaled longitudinal sum of expression
    % Expression_longitudinal is the *normalised* logintudinal sum of expression
    % cot5, hyp5 etc. are median levels of unscaled expression the 5*5 patches
    Expression = NaN(Nx, T); cot5=NaN(1,T);hyp5=NaN(1,T);root5=NaN(1,T);RT5=NaN(1,T); %initialise the arrays
   PlT=NaN(Nx,T);
    for k=1:T
        cc = (cos(X(:,:,k))+1);    
        cc = cc.*TemplateSave(:, :, k);
        Expression(:, k) = nansum(cc, 2);
       % last=max(find(max(TemplateSave(:, :, k)')==1));% ensure all cells below plant are NaNs.
        %Expression(last+1:end, k) = NaN;
        %% expression of 5 pixel *5 pixel sections added along 
        cot5(k)=nanmedian(reshape(cc(cotY, cotX), [numel(cc(cotY, cotX)) 1]));
        hyp5(k)=nanmedian(reshape(cc(hypY, width_of_Root), [numel(cc(hypY, width_of_Root)) 1]));
        root5(k)=nanmedian(reshape(cc(rootY, width_of_Root), [numel(cc(rootY, width_of_Root)) 1]));
        % root tip doe separately since we need to kep updating its position
        final=max(find(max(TemplateSave(:, :, k)')==1)); 
        RT5(k)=nanmedian(reshape(cc([final-4 final], width_of_Root), [numel(cc([final-4 final], width_of_Root)) 1]));
        %plant template: keep track
        PlT(cells_where_diffs(1)+1:final,k)=1;
    end
    Expression_longitudinal = diag(1./max(Expression(:,1:T)')) * Expression(:,1:T); 
    Expression_longitudinal=Expression_longitudinal.* PlT;
 %   Expression_longitudinal(Expression_longitudinal == 0) = NaN; 
    
    %% locations of peaks (loc) across 5 pixels of each tissue 
    [val, locC5]=findpeaks(cot5(Tmin:T), Tmin:T, 'MinPeakDistance', 12,'MinPeakProminence',1);
    [val, locH5]=findpeaks(hyp5(Tmin:T), Tmin:T, 'MinPeakDistance', 12,'MinPeakProminence',1);
    [val, locR5]=findpeaks(root5(Tmin:T), Tmin:T, 'MinPeakDistance',12, 'MinPeakProminence',1);
    [val, locRT5]=findpeaks(RT5(Tmin:T), Tmin:T, 'MinPeakDistance', 12, 'MinPeakProminence',1);
       
    %% periods from peak-to-peak values across 5 pixels of each tissue  
    perCot5=mean(diff(locC5));
    perHyp5=mean(diff(locH5));
    perRoot5=mean(diff(locR5));
    perRT5=mean(diff(locRT5));
    
    %% calculating the phase coherence; 
    % Nhyp, Ncot, NRT, Nroot1 are number of pixels (cells) in each tissue.
    % Ntot is the number of cells in the original template. 
    % for root, the number of pixels will change as the rot section grows and this is calculated later in Nroot. 
      Nhyp=sum(reshape(TemplateSave(tipHyp:Junction-1, width_of_Hyp, 1),...
          [numel(TemplateSave(tipHyp:Junction-1, width_of_Hyp, 1)) 1]));
      Ncot=sum(reshape(TemplateSave(1:Junction-1,:, 1)==1,...
          [numel(TemplateSave(1:Junction-1,:, 1)==1), 1]))-Nhyp;
      NRT=sum(reshape(TemplateSave(finalRT-4:finalRT, :, 1)==1,...
          [numel(TemplateSave(finalRT-4:finalRT, :, 1)==1), 1])); 
      Nroot1=sum(reshape(TemplateSave(Junction:finalRT, :, 1)==1,...
          [numel(TemplateSave(Junction:finalRT, :, 1)==1), 1]))-NRT; 
      Ntot=Nhyp+Ncot+Nroot1+NRT;
      % initialise arrays.
      rval_Hyp=NaN(1,T);rval_Cot=NaN(1,T);rval_Root=NaN(1,T);rval_RT=NaN(1,T);rval_Root_nongrow=NaN(1,T);rval=NaN(1,T);rval_nongrow=NaN(1,T);
   for k=1:T
      %number of pixels in the root will change with growth.
      final= max(find(max(TemplateSave(:, :, k)')==1));
      Nroot= sum(reshape(TemplateSave(Junction:final, :, k)==1,...
          [numel(TemplateSave(Junction:final, :, k)==1), 1]))-NRT;
      
      phase=X(:, :,k).*TemplateSave(:, :, k);
      cosPhase=cos(phase); sinPhase=sin(phase);
      % r values for  all cells (growing root)
      Ntot_grow=Nhyp+Ncot+NRT+Nroot; %number of cells in a growing root plant 
      rval(k)=1/Ntot_grow*sqrt((nansum(cosPhase(:)))^2+(nansum(sinPhase(:)))^2);
            
      %figure
      %imagesc(TemplateSave(tipHyp:final, :, k))
      % MG. calc. rval on Junction-16:end of RT as in experimental data.
      Nroot_and_some_hyp= sum(reshape(TemplateSave(tipHyp:final, 39:47, k)==1,...
        [numel(TemplateSave(tipHyp:final, 39:47, k)==1), 1]));
    
      cosPhase_root_and_some_hyp = cosPhase(tipHyp:final, 39:47); 
      sinPhase_root_and_some_hyp = sinPhase(tipHyp:final, 39:47);    
      rval_Root(k)=1/Nroot_and_some_hyp*sqrt((nansum(cosPhase_root_and_some_hyp(:)))...
          ^2+(nansum(sinPhase_root_and_some_hyp(:)))^2);
      
      %% r of all cells (without growing part of root)
      cRT=nansum(reshape(cosPhase(final-4:final, :),...
          [numel(cosPhase(final-4:final, :)), 1])); 
      sRT=nansum(reshape(sinPhase(final-4:final, :),...
          [numel(sinPhase(final-4:final, :)), 1]));
      c_nongrow=nansum(reshape(cosPhase(1:finalRT-5, :),...
          [numel(cosPhase(1:finalRT-5, :)), 1]))+ cRT;
      s_nongrow=nansum(reshape(sinPhase(1:finalRT-5, :),...
          [numel(sinPhase(1:finalRT-5, :)), 1]))+sRT;
      rval_nongrow(k)=1/Ntot*sqrt((c_nongrow)^2+(s_nongrow)^2);
   
   end

%% Kymograph    
    Fig=figure;
    % Junction cell is the 0 position (first root cell). TipHyp is top hypocotyl cell
    K=imagesc(Expression_longitudinal(Junction-16:end,Tmin:T));
    set(K, 'AlphaData', ~isnan(Expression_longitudinal(Junction-16:end,Tmin:T)))
    
    set(gca, 'Fontsize', 7, 'yTick', [1,Junction-(Junction-16)+1:30:Nx], 'yTickLabel', [-16, 0:30:Nx-(tipHyp+5)],'xTick', 1:Per:T, 'xTickLabel', Tmin:Per:T)
    colorbar('Ticks', [min(min(Expression_longitudinal(Junction-16:end,Tmin:T))) max(max(Expression_longitudinal(Junction-16:end,Tmin:T)))], 'TickLabels', {'min', 'max'} )
    ylabel({'Distance from hypocotyl', 'junction (a.u.)'});
    xlabel(['Time in ' name(3:4) '(h)'])
    set(Fig,'PaperUnits', 'centimeters',  'PaperPosition', [0 0  3000 1200]/300)
    %print('-painters', Fig, ['AltModel_Factor=' num2str(Factor) 'Omegastdev=' num2str(omega_stdev)], '-dpdf','-r300')
    
   % print('-painters', Fig, [name 'AltModel'  num2str(runs)], '-dpdf','-r300')
    %% Movie of the plant gene expression (uncomment these lines to see the movie, only in not using parallel pool)
%    clf
%     for k=1:T
%         cc = (cos(X(:,:,k))+1);    
%         cc = cc.*TemplateSave(:, :, k);
%         imagesc(cc)
%         truesize([239*3 84*3])
%         Movie(k) = getframe(gcf);
%     end
%     clf
%     movie(gcf,Movie)
%% Information saved to the MAT file
    % (1) Expression longitudinal, normalised; rows are longitudinal position, columns are time (in hours)
    % (2) X : expression
    % (3) perCot5, perHyp5, perRoot5, perRT5 : period of median expression
    % of 5*5 pixel patches
    % (4) locC5,locH5, locR5, locRT5: location of phases of median expression of 5*5 pixel patches
    % (5) rval:  Kuramoto phase order parameters for growing plant and rval_nongrow: Kuramoto phase order parameters for plant parts which are not growing. 
 
    parsave(sprintf(['model/', name, '/', name '_run%d.mat'], runs), Expression_longitudinal, X, perCot5, perHyp5, perRoot5, perRT5, locC5,locH5, locR5, locRT5,rval, rval_nongrow, rval_Root)

    %parsave(sprintf( ['AltModel_Factor=' num2str(Factor) 'Omegastdev=' num2str(omega_stdev) '.mat'], runs), Expression_longitudinal, X, perCot5,perHyp5, perRoot5, perRT5, locC5,locH5, locR5, locRT5,rval, rval_nongrow )
    
    %parsave(sprintf([name 'AltmodelPhase2hPeriodsNotNoisy_run%d.mat'], runs), Expression_longitudinal, X, perCot5,perHyp5, perRoot5, perRT5, locC5,locH5, locR5, locRT5,rval, rval_nongrow )
    
end
toc

% 
function parsave(fname, Expression_longitudinal, X, perCot5,perHyp5, perRoot5, perRT5, locCot5,locHyp5, locRoot5, locRT5, rval, rval_nongrow, rval_Root)
 save(fname, 'Expression_longitudinal', 'X', 'perCot5','perHyp5', 'perRoot5', 'perRT5', 'locCot5','locHyp5', 'locRoot5', 'locRT5', 'rval', 'rval_nongrow', 'rval_Root') % 'locR', 'locRT')
end